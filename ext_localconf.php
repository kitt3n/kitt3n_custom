<?php
defined('TYPO3_MODE') || die('Access denied.');


## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$boot = function () {

    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('kitt3n_custom')) {

//        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginLogo'] = 'EXT:kitt3n_custom/Resources/Public/Assets/Backend/loginLogo.svg';
//        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginHighlightColor'] = '#000000';
//        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginBackgroundImage'] = 'EXT:kitt3n_custom/Resources/Public/Assets/Backend/loginBackgroundImage.jpg';
//        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['loginFootnote'] = '© ZWEI14 GmbH';
//        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['backendLogo'] = 'EXT:kitt3n_custom/Resources/Public/Assets/Backend/backendLogo.svg';
//        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['backend']['backendFavicon'] = 'EXT:kitt3n_custom/Resources/Public/Assets/Backend/backendFavicon.ico';

        ## Add default page tsconfig
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="DIR:EXT:kitt3n_custom/Configuration/TSconfig/Page" extensions="ts,txt,typoscript">');

        ## Add default user tsconfig
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('<INCLUDE_TYPOSCRIPT: source="DIR:EXT:kitt3n_custom/Configuration/TSconfig/User" extensions="ts,txt,typoscript">');

        ## Include custom RTE settings
        $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['custom'] = 'EXT:kitt3n_custom/Configuration/RTE/editor.yaml';

    }

};

$boot();
unset($boot);
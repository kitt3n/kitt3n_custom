<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('kitt3n_custom', 'Configuration/TypoScript', 'KITT3N | Custom');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

/***************
 * Add own stylesheet(s) to TYPO3 backend, tested with TYPO3 8.7
 * Fügt eigene CSS-Dateien zum TYPO3-Backend hinzu; getestet mit TYPO3 8.7
 */
$GLOBALS['TBE_STYLES']['skins']['kitt3n_custom'] = array();
$GLOBALS['TBE_STYLES']['skins']['kitt3n_custom']['name'] = 'MyExtension AddOn';
$GLOBALS['TBE_STYLES']['skins']['kitt3n_custom']['stylesheetDirectories'] = array(
    'EXT:kitt3n_custom/Stylesheets/Structure'
);